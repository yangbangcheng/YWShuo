/* eslint valid-jsdoc: "off" */

'use strict';
const path = require('path');

/**
 * @param {Egg.EggAppInfo} appInfo app info
 */
module.exports = appInfo => {
    /**
     * built-in config
     * @type {Egg.EggAppConfig}
     **/
    const config = exports = {};

    // ORM框架配置信息
    config.sequelize = {
        dialect: 'mysql',
        host: '127.0.0.1',
        port: '3306',
        user: 'root',
        password: '123456',
        database: 'novel_review',
        charset: "utf8mb4",
        define: {
            // 锁定表名和模型名一致，不自动转为复数
            freezeTableName: true,
        },
    };

    // add your user config here
    const userConfig = {
        // 在这个数组里面的api会被打印日志
        LogApiList: []
    };

    return {
        ...config,
        ...userConfig,
    };
};
