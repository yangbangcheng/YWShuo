/* eslint valid-jsdoc: "off" */

'use strict';
const path = require('path');
/**
 * @param {Egg.EggAppInfo} appInfo app info
 */
module.exports = appInfo => {
    /**
     * built-in config
     * @type {Egg.EggAppConfig}
     **/
    const config = exports = {};

    // use for cookie sign key, should change to your own and keep security
    config.keys = appInfo.name + '_1616548733388_7125';

    // add your middleware config here
    config.middleware = [];

    // ORM框架配置信息
    // config.sequelize = {
    //     dialect: 'mysql',
    //     host: '121.5.71.198',
    //     port: '3306',
    //     username: 'novel_review',
    //     password: 'G8BcDJiLmFakCJ8r',
    //     database: 'novel_review',
    //     charset: "utf8mb4",
    //     define: {
    //         // 锁定表名和模型名一致，不自动转为复数
    //         freezeTableName: true,
    //     },
    // };
    // 安全配置
    config.security = {
        csrf: {
            enable: false,
        }
    }
    // jwt配置
    config.jwt = {
        secret: config.keys,	//自定义token的加密条件字符串，可按各自的需求填写
    };
    // redis配置
    config.redis = {
        client: {
            port: 6379,
            host: '121.5.71.198',
            password: '123456',
            db: 0
        }
    }

    // 多进程启动
    config.cluster = {
        listen: {
            path: '',
            port: 8038,
            hostname: '127.0.0.1',
        }
    };

    // 日志配置
    config.logger = {
        appLogName: `egg-web.log`,
        coreLogName: 'egg-web.log',
        agentLogName: 'egg-agent.log',
        errorLogName: 'egg-error.log',
        outputJSON: true,
    }
    // 日志切割配置
    config.logrotator = {
        filesRotateBySize: [
            path.join(appInfo.root, 'logs', 'ywsServer', 'web-server.log'),
        ],
        // 20 * 1024 * 1024
        maxFileSize: 20971520,
    }
    // 自定义日志
    config.customLogger = {
        webServer: {
            file: path.join(appInfo.root, 'logs/ywsServer/web-server.log'),
        }
    }

    // add your user config here
    const userConfig = {
        // 在这个数组里面的api会被打印日志
        LogApiList: ['/api/novel/getList']
    };

    return {
        ...config,
        ...userConfig,
    };
};
