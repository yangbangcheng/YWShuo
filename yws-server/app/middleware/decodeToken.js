module.exports = (options) => {
    return async function (ctx, next) {
        let token = ctx.request.header.authorization//拿到token
        //如果token存在
        if (token) {
            try {
                // 解码token
                const {secret} = ctx.app.config.jwt
                ctx.token = ctx.app.jwt.verify(token, secret);
                await next();
            } catch (error) {
                ctx.token = {}
                await next();
            }
        } else {
            ctx.token = {}
            await next();
        }
    }
}