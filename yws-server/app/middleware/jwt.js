module.exports = (options) => {
    return async function (ctx, next) {
        let token = ctx.request.header.authorization//拿到token
        //如果token存在
        if (token) {
            const webServerLog = ctx.getLogger('webServer')
            try {
                // 解码token
                const {secret} = ctx.app.config.jwt
                ctx.token = ctx.app.jwt.verify(token, secret);
                
                webServerLog.info('用户校验成功: %j', ctx.token)
                await next();
            } catch (error) {
                webServerLog.info('用户校验失败: %j', error)
                ctx.body = {
                    error: '01',
                    msg: '登录过期'
                };
                return;
            }
        } else {
            ctx.body = {
                error: '01',
                msg: '请先登录'
            }
        }
    }
}