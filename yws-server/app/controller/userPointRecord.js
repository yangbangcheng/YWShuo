'use strict';

const Controller = require('egg').Controller;

class UserPointRecordController extends Controller {
    async getList() {
        const { query, service } = this.ctx;
        const userId = this.ctx.token.userId
        const { page = 1, num = 20 } = query
        const params = {
            page,
            num,
            userId
        }

        return await service.userPointRecord.getList(params);
    }
}

module.exports = UserPointRecordController;
