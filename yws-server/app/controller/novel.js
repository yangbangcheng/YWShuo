'use strict';

const Controller = require('egg').Controller;

class NovelController extends Controller {
    // 搜索
    async search() {
        const {query, service} = this.ctx;
        if (query.keyword == '') throw {code: '02', msg: '请输入关键字'}
        const params = {
            keyword: query.keyword
        }
        return await service.novel.search(params)
    }
    // 获取列表
    async getList() {
        const {query, service} = this.ctx;
        const params = {
            page: query.page || 1,
            num: query.num || 20,
            categoryId: query.categoryId || '',
            updateStatus: query.updateStatus || '',
            type: query.type || '',
            update: query.update || '',
            countWord: query.countWord || '',
            sort: query.sort || 'power',
            tag: query.tag || '',
        }

        // let result = await service.redis.get(JSON.stringify(params));
        // if (!result) {
        const result = await service.novel.getList(params)
        //     service.redis.set(JSON.stringify(params), result, 1800);
        // }

        return result;
    }
    // 获取分类
    async getCategory() {
        const {query, service} = this.ctx;
        const params = {
            type: query.type || ''
        }

        // let result = await service.redis.get(JSON.stringify(params));
        // if (!result) {
        const result = await service.novel.getCategory(params)
        //     service.redis.set(JSON.stringify(params), result, 3600);
        // }

        return result;
    }
    // 获取详情
    async getInfo() {
        const {query, service} = this.ctx;
        const params = {
            id: query.novelId
        }
        
        return await service.novel.getInfo(params);
    }
    // 获取评论列表
    async getDiscussList() {
        const {query, service} = this.ctx;
        const params = {
            id: query.novelId,
            score: query.score || 2, // 1只展示有评分的，2只展示无评分的, 0展示全部
            page: query.page || 1,
            num: query.num || 20,
            sort: query.sort || 'power'
        }
        
        return await service.novel.getDiscussList(params);
    }
    // 随机获取评论
    async getRandomDiscuss() {
        const {query, service} = this.ctx;
        const params = {
            score: 1,
            page: query.page || 1,
            num: 20
        }
        const redisName = JSON.stringify(params) + 'getRandomDiscuss'
        let result = await service.redis.get(redisName);
        if (!result) {
            result = await service.novel.getRandomDiscuss(params)
            service.redis.set(redisName, result, 600);
        }
        return result;
    }
    // 获取用户评论详情
    async getDiscussInfo() {
        const {query, service} = this.ctx;
        const userId = this.ctx.token.userId
        const params = {
            novelId: query.novelId,
            userId: userId
        }
        
        return await service.novel.getDiscussInfo(params);
    }
    // 评论
    async postDiscuss() {
        const {service} = this.ctx;
        const data = this.ctx.request.body
        if (!data.novelId || !data.type || !data.content) throw { code: '02', msg: '缺少参数' }
        const userId = this.ctx.token.userId
        const params = {
            novelId: data.novelId,
            type: data.type, // 1 发布评论(评分和吐槽) 2修改评分
            score: data.score*2 || null,
            content: data.content,
            userId: userId
        }
        
        return await service.novel.postDiscuss(params);
    }

    // 修改书籍标签
    async editTag() {
        const {service} = this.ctx;
        const userId = this.ctx.token.userId
        const {novelId, type, tagName} = this.ctx.request.body
        if (!novelId || !type || !tagName) throw { code: '02', msg: '缺少参数' }

        const params = {
            userId,
            novelId,
            type, // 1新增 其余删除
            tagName
        }

        return await service.novel.editTag(params);
    }

    // 获取用户小说标签
    async getUserTagList() {
        const {query, service} = this.ctx;
        const userId = this.ctx.token.userId
        const params = {
            novelId: query.novelId,
            userId: userId
        }
        
        return await service.novel.getUserTagList(params);
    }

    // 书籍打赏
    async addPoint() {
        const { service } = this.ctx;
        const userId = this.ctx.token.userId
        const { target = 1, targetId, changeNum } = this.ctx.request.body

        if (!targetId || !changeNum) throw { code: '02', msg: '缺少参数' }

        const params = {
            userId,
            target,
            targetId,
            type: 2,
            changeNum,
            desc: '打赏书籍'
        }

        return await service.userPointRecord.addRecord(params);
    }
}

module.exports = NovelController;
