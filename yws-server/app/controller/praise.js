'use strict';

const Controller = require('egg').Controller;

class PraiseController extends Controller {
    // 点赞或点赞
    async setStatus() {
        const {service} = this.ctx
        const data = this.ctx.request.body
        const {type = 1, targetId, target =1} = data
        if (!targetId) throw {code: '02', msg: '缺少必要参数'}
        const userId = this.ctx.token.userId

        const params = {
            type,
            targetId,
            target,
            userId
        }
        console.log(params)
        return await service.praise.setStatus(params);
    }
}

module.exports = PraiseController;
