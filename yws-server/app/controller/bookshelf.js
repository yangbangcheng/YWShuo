'use strict';

const Controller = require('egg').Controller;

class BookshelfController extends Controller {
    // 加入或者移除书架
    async setStatus() {
        const {service} = this.ctx
        const data = this.ctx.request.body
        const {type = 1, status = 1, novelId} = data
        // type 为移除类型 后续要修改，已经有status控制了
        if (!novelId) throw {code: '02', msg: '缺少必要参数'}
        const userId = this.ctx.token.userId

        const params = {
            type,
            status,
            userId,
            novelId: novelId*1
        }

        return await service.bookshelf.setStatus(params);
    }
    // 获取书架书籍
    async getNoverList() {
        const {query, service} = this.ctx
        const {type = 1, status = 1, page = 1, num = 20} = query
        const userId = this.ctx.token.userId

        const params = {
            type,
            status,
            userId,
            page,
            num
        }

        return await service.bookshelf.getNoverList(params);
    }
    // 获取书籍在书架的状态
    async getNoverStatus() {
        const {query, service} = this.ctx
        const userId = this.ctx.token.userId
        const {novelId} = query

        return await service.bookshelf.getNoverStatus({userId, novelId}) || {};
    }
}

module.exports = BookshelfController;
