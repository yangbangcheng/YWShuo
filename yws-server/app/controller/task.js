'use strict';

const Controller = require('egg').Controller;

class TaskController extends Controller {
    // 获取任务列表
    async getList() {
        const { service } = this.ctx;
        const userId = this.ctx.token.userId
        return await service.task.getList({userId});
    }
    // 签到任务
    async signIn() {
        const { service } = this.ctx;
        const { taskId = 1 , desc = '签到任务'} = this.ctx.request.body
        const userId = this.ctx.token.userId
        try {
            await service.task.signIn({taskId, userId, desc});
            return '签到成功'
        } catch (error) {
            throw {code: '02', msg: error.msg || error}
        }
    }
}

module.exports = TaskController;
