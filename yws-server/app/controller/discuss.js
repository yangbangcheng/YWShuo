'use strict';

const Controller = require('egg').Controller;

class DiscussController extends Controller {
    // 获取回复
    async getReply() {
        const {query, service} = this.ctx;
        const params = {
            id: query.discussId,
            res_id: query.resId || null,
            page: query.page || 1,
            num: query.num || 10
        }
        
        return await service.discuss.getReply(params);
    }
    // 发布评论
    async postReply() {
        const {service} = this.ctx
        const data = this.ctx.request.body
        const userId = this.ctx.token.userId
        const params = {
            parent_id: data.parentId,
            novel_id: data.novelId,
            content: data.content,
            user_id: userId,
            res_id: data.resId || null,
        }
        return await service.discuss.postReply(params);
    }
}

module.exports = DiscussController;
