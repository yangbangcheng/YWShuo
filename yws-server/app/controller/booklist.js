'use strict';

const Controller = require('egg').Controller;

class BooklistController extends Controller {
    // 获取列表
    async getList() {
        const {query, service} = this.ctx;
        const {page = 1, num = 20, type = 1, sort = 'power', userId} = query

        const params = {
            page,
            num,
            type,
            sort,
            userId
        }

        // let result = await service.redis.get(JSON.stringify(params));
        // if (!result) {
        const result = await service.booklist.getList(params)
        //     service.redis.set(JSON.stringify(params), result, 1800);
        // }
        // console.log(result.data[0]);
        return result
    }

    // 获取书单信息
    async getInfo() {
        const {query, service} = this.ctx;
        const {page = 1, num = 20, booklistId} = query

        const params = {
            page,
            num,
            booklistId
        }

        return await service.booklist.getInfo(params)
    }

    // 创建书单
    async create() {
        const {service} = this.ctx
        const data = this.ctx.request.body
        const userId = this.ctx.token.userId
        const params = {
            user_id: userId,
            title: data.title,
            type: data.type,
            intro: data.intro,
        }
        const result = await service.booklist.create(params);

        return result;
    }
    
    // 添加书籍
    async addNovel() {
        const {service, model} = this.ctx
        const data = this.ctx.request.body
        let {booklistId, discussId, categoryId, novelId, score, content} = data
        const userId = this.ctx.token.userId
        // 有评论才查询
        if (discussId) {
            // 查询书籍是否在书单里面存在
            const sql = {
                where: {
                    booklist_id: booklistId,
                    novel_id: novelId,
                    user_id: userId,
                }
            }
            const novelInfo =  await model.BooklistNovel.findOne(sql)
            if (novelInfo) throw {code: '02', msg: '该书籍已被添加到此书单中'}
        } else {
            // 没有评论创建评分
            const params = {
                novelId,
                type: 1, // 1 发布评论 2修改评论
                score: score*2 || null,
                content,
                userId: userId
            }
            const res = await service.novel.postDiscuss(params);
            discussId = res.id
        }

        let params = {
            booklist_id: booklistId,
            novel_id: novelId,
            user_id: userId,
            discuss_id: discussId,
            category_id: categoryId
        }
        return await service.booklist.addNovel(params);
    }

    // 获取书单书籍列表
    async getNovelList() {
        const {query, service} = this.ctx;
        const {userId} = this.ctx.token
        const params = {
            user_id: userId || null,
            booklist_id: query.booklistId,
            page: query.page || 1,
            num: query.num || 20,
        }

        // let result = await service.redis.get(JSON.stringify(params));
        // if (!result) {
        const result = await service.booklist.getNovelList(params)
        //     service.redis.set(JSON.stringify(params), result, 1800);
        // }

        return result;
    }

    // 获取书单评论
    async getDiscussList() {
        const {query, service} = this.ctx;
        const params = {
            booklist_id: query.booklistId,
            novel_id: query.novelId,
            page: query.page || 1,
            num: query.num || 20,
        }

        // let result = await service.redis.get(JSON.stringify(params));
        // if (!result) {
        const result = await service.booklist.getDiscussList(params)
        //     service.redis.set(JSON.stringify(params), result, 1800);
        // }

        return result;
    }

    // 创建书单评论
    async createDiscuss() {
        const {service} = this.ctx
        const data = this.ctx.request.body
        // const userId = this.ctx.token.userId
        const userId = data.userId
        // 查询书籍是否在书单里面存在
        let params = {
            booklist_id: data.booklistId,
            novel_id: data.novelId,
        }
        const novelInfo = await service.booklistNovel.getInfo(params);
        if (!novelInfo) throw {code: '02', msg: '该书籍未被添加到此书单中'}

        params = {
            booklist_id: data.booklistId,
            novel_id: data.novelId,
            user_id: userId,
            discuss_id: data.discussId || null,
            res_id: data.resId || null,
            content: data.content
        }
        
        return await service.booklist.createDiscuss(params);
    }
}

module.exports = BooklistController;

