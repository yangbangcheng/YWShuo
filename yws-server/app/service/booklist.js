'use strict';

const Service = require('egg').Service;
const { Sequelize, Op }  = require("sequelize");

class BooklistService extends Service {
    // 书单列表
    async getList(params) {
        const { model } = this.app
        const {userId} = params

        let sql = {}, orderSql = {}

        // 排序
        let sortStr = params.sort

        switch (params.sort) {
            case 'time':
                sortStr = 'update_time'
                break;
            // 硬币
            case 'point':
                sortStr = 'point'
                break;
        }

        orderSql = [[sortStr, 'DESC']]
        let whereSql = {
            type: params.type,
            status: 1
        }
        if (userId) whereSql.user_id = userId
        sql = {
            where: whereSql,
            order: orderSql,
            include: [{
                as: 'userInfo',
                model: model.User,
                attributes: ['name']
            }, {
                as: 'categorys',
                model: model.BooklistCategory
            }],
            limit: params.num * 1, // 返回数据量
            offset: params.num * (params.page - 1), // 数据偏移量
        }

        let { count, rows } = await model.Booklist.findAndCountAll(sql);
        const pageAll = Math.ceil(count / params.num)

        return {
            page: params.page,
            pageAll: pageAll,
            data: rows
        }
    }
    // 书单信息
    async getInfo(params) {
        const { model } = this.app
        const {booklistId} = params
        const sql = {
            where: { id: booklistId},
            include: [{
                as: 'userInfo',
                model: model.User,
                attributes: ['name']
            }, {
                as: 'categorys',
                model: model.BooklistCategory
            }],
        }
        return await model.Booklist.findOne(sql)
    }

    // 获取书籍列表
    async getNovelList(params) {
        const { model } = this.app
        const { booklist_id, user_id } = params
        let sql = {}, orderSql = {}

        const whereSql = {
            booklist_id
        }

        orderSql = [['update_time', 'DESC']]

        sql = {
            where: whereSql,
            order: orderSql,
            include: [{
                as: 'discussInfo',
                model: model.NovelDiscuss,
                attributes: {
                    include: [
                        [Sequelize.literal(`(SELECT COUNT(*) FROM praise WHERE praise.target_id = discussInfo.id AND praise.type = 1 AND praise.status = 1)`), 'dz_num'],
                        [Sequelize.literal(`(SELECT COUNT(*) FROM praise WHERE praise.target_id = discussInfo.id AND praise.type = 2 AND praise.status = 1)`), 'c_num'],
                        [Sequelize.literal(`(SELECT COUNT(*) FROM novel_discuss AS Discuss WHERE Discuss.parent_id = discussInfo.id)`), 'reply_num'],
                    ]
                },
            }, {
                as: 'novel',
                model: model.Novel,
                attributes: {
                    include: [
                        // userid 为null的数据 实际不存在，所以可以这么干，但最好修复
                        [Sequelize.literal(`(SELECT COUNT(*) FROM bookshelf WHERE bookshelf.novel_id = novel.id AND bookshelf.status = 1 AND bookshelf.user_id = ${user_id})`), 'bookshelf_status'],
                    ]
                },
            }],
            limit: params.num * 1, // 返回数据量
            offset: params.num * (params.page - 1), // 数据偏移量
        }

        const { count, rows } = await model.BooklistNovel.findAndCountAll(sql);

        const pageAll = Math.ceil(count / params.num)
        return {
            page: params.page,
            pageAll: pageAll,
            data: rows
        }
    }
    // 创建书单
    async create(params) {
        const { model } = this.app

        const userId = params.user_id
        const sql = {
            user_id: userId,
            title: params.title,
            type: params.type,
            intro: params.intro,
            create_time: (new Date).getTime() / 1000,
            update_time: (new Date).getTime() / 1000
        }
        const result = await model.Booklist.create(sql)
        return result
    }
    // 添加书籍
    async addNovel(params) {
        const {user_id, booklist_id, novel_id, discuss_id, category_id} = params
        const { model } = this.app
        
        // 建立事务对象
        let transaction;
        try {
            transaction = await model.transaction();

            let sql = {
                where: {
                    id: category_id
                },
                transaction
            }

            let result = await model.NovelCategory.findOne(sql)

            if (!result) throw { code: '02', msg: '没有该分类' }

            sql = {
                where: {
                    booklist_id,
                    category_id,
                    category_name: result.cate_name,
                },
                defaults: {
                    count: 1,
                    create_time: (new Date).getTime() / 1000,
                    update_time: (new Date).getTime() / 1000
                },
                transaction
            }

            result = await model.BooklistCategory.findOrCreate(sql)
            
            // 已存在
            if (!result[1]) {
                // 事务改操作
                await model.BooklistCategory.update({
                    count: model.literal('count+1') 
                }, {
                    where: {
                        booklist_id,
                        category_id,
                    },
                    transaction
                });
            }

            sql = {
                user_id,
                booklist_id,
                novel_id,
                discuss_id,
                category_id,
                create_time: (new Date).getTime() / 1000,
                update_time: (new Date).getTime() / 1000,
            }
            result = await model.BooklistNovel.create(sql, {transaction})
            // 提交事务
            await transaction.commit();
            return result

        } catch (error) {
            // 事务回滚
            await transaction.rollback();
            throw error;
        }
        
    }

    // 获取书单评论
    async getDiscussList(params) {
        const { model } = this.app

        let sql = {}, whereSql = {}

        whereSql = {
            booklist_id: params.booklist_id,
            novel_id: params.novel_id,
        }

        sql = {
            where: whereSql,
            include: [{
                as: 'userInfo',
                model: model.User,
                attributes: ['name']
            }, {
                as: 'resInfo',
                model: model.User,
                attributes: ['name']
            }],
            limit: params.num * 1, // 返回数据量
            offset: params.num * (params.page - 1), // 数据偏移量
        }

        const { count, rows } = await model.BooklistDiscuss.findAndCountAll(sql);

        const pageAll = Math.ceil(count / params.num)
        return {
            page: params.page,
            pageAll: pageAll,
            data: rows
        }
    }
    // 创建书籍评论
    async createDiscuss(params) {
        const { model } = this.app

        const sql = {
            user_id: params.user_id,
            booklist_id: params.booklist_id,
            novel_id: params.novel_id,
            discuss_id: params.discuss_id,
            res_id: params.res_id,
            content: params.content,
            create_time: (new Date).getTime() / 1000,
            update_time: (new Date).getTime() / 1000
        }
        const result = await model.BooklistDiscuss.create(sql)
        return result
    }

}

module.exports = BooklistService;
