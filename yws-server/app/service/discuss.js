'use strict';

const Service = require('egg').Service;
const { Op } = require("sequelize");

class DiscussService extends Service {
    // 获取评论列表
    async getReply(params) {
        const {model} = this.app

        let sql = {}, whereSql = {}, orderSql = {}

        whereSql.parent_id = params.id
        if(params.res_id) whereSql.res_id = params.res_id
        
        orderSql = [['update_time', 'DESC']]

        sql = {
            where: whereSql,
            order: orderSql,
            include: [{
                as: 'userInfo',
                model: model.User,
                attributes: ['name']
            }, {
                as: 'resInfo',
                model: model.User,
                attributes: ['name']
            }],
            limit: params.num * 1, // 返回数据量
            offset: params.num * (params.page - 1), // 数据偏移量
        }

        const { count, rows } = await model.NovelDiscuss.findAndCountAll(sql);

        const pageAll = Math.ceil(count / params.num)
        return {
            page: params.page,
            pageAll: pageAll,
            data: rows
        }
    }
    // 获取评论信息
    async getInfo(params) {
        const {model} = this.app

        let sql = {}, whereSql = {}

        whereSql={
            novel_id: params.novel_id,
            user_id: params.user_id,
        }
        
        if (params.isScore) whereSql.score = {[Op.ne]: null}

        sql = {
            where: whereSql
        }

        const result = await model.NovelDiscuss.findOne(sql);
        return result
    }
    // 发布评论
    async postReply(params) {
        const {model} = this.app

        const userId = params.user_id
        const userInfo = await model.User.findByPk(userId)
        const sql = {
            parent_id: params.parent_id,
            novel_id: params.novel_id,
            user_id: userId,
            res_id: params.res_id,
            score: params.score || null,
            score_level: userInfo.score_level,
            content: params.content,
            create_time: (new Date).getTime()/1000,
            update_time: (new Date).getTime()/1000
        }

        // 建立事务对象
        let transaction;
        try {
            transaction = await model.transaction();

            let whereSql = {}
            if (params.res_id != 'NULL') {
                whereSql = {
                    [Op.or]: [{
                        id: params.parent_id
                    }, {
                        id: params.res_id
                    }]
                }
            } else {
                whereSql = {
                    id: params.parent_id
                }
            }
            // 事务改操作
            await model.NovelDiscuss.update({
                reply_num: model.literal('reply_num+1') 
            }, {
                where: whereSql,
                transaction: transaction,
            });

            const result = await model.NovelDiscuss.create(sql,{transaction: transaction})
            
            // 提交事务
            await transaction.commit();

            return result
        } catch (error) {
            // 事务回滚
            await transaction.rollback();
            throw error;
        }
    }
}

module.exports = DiscussService;
