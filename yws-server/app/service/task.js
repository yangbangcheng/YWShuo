'use strict';

const Service = require('egg').Service;

class TaskService extends Service {
    async getList(params) {
        const {model} = this.app
        const {userId} = params
        const zeroTime = new Date().setHours(0, 0, 0, 0)/1000
        const sql = {
            attributes: {
                include: [
                    [Sequelize.literal(`(SELECT * FROM task_record WHERE task_record.user_id = ${userId} AND task_record.task_id = task.id AND task_record.update_time >= ${zeroTime})`), 'task_count'],
                ]
            },
        }

        return await model.Task.findAll(sql);
    }
    // 签到任务
    async signIn(params) {
        const {model} = this.app
        const {service} = this.ctx
        let {userId, taskId, desc} = params
        const zeroTime = new Date().setHours(0, 0, 0, 0)/1000
        // 奖励的基础值
        let result = await model.Task.findByPk(taskId)
        if (!result) throw '没有添加该任务'
        const point = result.point
        
        let sql = {
            where: {
                user_id: userId,
                task_id: taskId
            },
            defaults: {
                user_id: userId,
                task_id: taskId,
                point,
                desc,
                count: 1,
                continue_count: 1,
                create_time: (new Date).getTime() / 1000,
                update_time: (new Date).getTime() / 1000
            }
        }
        result = await model.TaskRecord.findOrCreate(sql)
        // 已存在更新数据
        if (!result[1]) {
            if (result[0].update_time >= zeroTime) throw {msg: '今日已签到'}

            const isContinue = zeroTime - result[0].update_time > 24*60*60 ? false : true

            const updateSql = {
                count: model.literal('count+1'),
                continue_count: isContinue ? model.literal('continue_count+1') : 1,
                update_time: (new Date).getTime() / 1000
            }
            sql = {
                where: {
                    user_id: userId,
                    task_id: taskId
                },
            }
            try {
                result = await model.TaskRecord.update(updateSql, sql);
                if (result.continue_count > 6) point += 2
            } catch (error) {
                throw error;
            }
        }

        // 用户加积分和相应的流水记录
        const pointRecordparams = {
            userId,
            target: 2,
            targetId: taskId,
            type: 1,
            changeNum: point,
            desc
        }
        try {
            await service.userPointRecord.addRecord(pointRecordparams);
        } catch (error) {
            throw error;
        }

        return
    }
    // 评分任务
    async discuss(params) {
        const {model} = this.app
        const {service} = this.ctx
        let {userId, taskId, desc} = params

        // 奖励的基础值
        let result = await model.Task.findByPk(taskId)
        if (!result) throw '没有添加该任务'
        const point = result.point
        
        let sql = {
            where: {
                user_id: userId,
                task_id: taskId
            },
            defaults: {
                user_id: userId,
                task_id: taskId,
                point,
                desc,
                count: 1,
                continue_count: 1,
                create_time: (new Date).getTime() / 1000,
                update_time: (new Date).getTime() / 1000
            }
        }
        result = await model.TaskRecord.findOrCreate(sql)
        // 已存在更新数据
        if (!result[1]) {
            const updateSql = {
                count: model.literal('count+1')
            }
            sql = {
                where: {
                    user_id: userId,
                    task_id: taskId
                },
            }
            try {
                result = await model.TaskRecord.update(updateSql, sql);
            } catch (error) {
                throw error;
            }
        }

        // 用户加积分和相应的流水记录
        const pointRecordparams = {
            userId,
            target: 2,
            targetId: taskId,
            type: 1,
            changeNum: point,
            desc
        }
        try {
            await service.userPointRecord.addRecord(pointRecordparams);
        } catch (error) {
            throw error;
        }

        return
    }
}

module.exports = TaskService;
