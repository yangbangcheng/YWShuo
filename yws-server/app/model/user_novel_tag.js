module.exports = app => {
    // 获取数据类型
    const DataTypes = app.Sequelize;

    // 定义模型
    const UserNovelTag = app.model.define('user_novel_tag', {
        id: {
            autoIncrement: true,
            type: DataTypes.INTEGER,
            allowNull: false,
            primaryKey: true,
            comment: "自增id"
        },
        tag_name: {
            type: DataTypes.STRING(64),
            allowNull: false,
            comment: "标签名"
        },
        novel_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            comment: "书籍id"
        },
        user_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            comment: "用户编号"
        },
        status: {
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 1,
            comment: "状态：1显示 0废弃"
        },
    }, {
        tableName: 'user_novel_tag',
        timestamps: false,
        indexes: [
            {
                name: "PRIMARY",
                unique: true,
                using: "BTREE",
                fields: [
                    { name: "id" },
                ]
            },
        ]
    });

    return UserNovelTag;
};