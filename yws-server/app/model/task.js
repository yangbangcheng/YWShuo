// 任务

module.exports = app => {
    // 获取数据类型
    const DataTypes = app.Sequelize;

    // 定义模型
    const Task = app.model.define('task', {
        id: {
            autoIncrement: true,
            type: DataTypes.INTEGER,
            allowNull: false,
            primaryKey: true,
            comment: "自增id"
        },
        title: {
            type: DataTypes.STRING(255),
            allowNull: false,
            comment: "名称"
        },
        desc: {
            type: DataTypes.TEXT,
            allowNull: false,
            comment: "内容描述"
        },
        point: {
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 0,
            comment: "奖励的积分"
        },
        create_time: {
            type: DataTypes.INTEGER,
            allowNull: false,
            comment: "创建时间"
        }
    }, {
        tableName: 'task',
        timestamps: false,
        indexes: [
            {
                name: "PRIMARY",
                unique: true,
                using: "BTREE",
                fields: [
                    { name: "id" },
                ]
            },
        ]
    });

    return Task;
};