// 书单 

module.exports = app => {
    // 获取数据类型
    const DataTypes = app.Sequelize;

    // 定义模型
    const Booklist = app.model.define('booklist', {
        id: {
            autoIncrement: true,
            type: DataTypes.INTEGER,
            allowNull: false,
            primaryKey: true,
            comment: "自增id"
        },
        user_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            comment: "用户编号"
        },
        title: {
            type: DataTypes.STRING(128),
            allowNull: false,
            comment: "书单名"
        },
        type: {
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 1,
            comment: "1男频 2女频"
        },
        intro: {
            type: DataTypes.STRING(256),
            allowNull: false,
            comment: "简介"
        },
        power: {
            type: DataTypes.STRING(32),
            allowNull: false,
            defaultValue: "0",
            comment: "权重"
        },
        point: {
            type: DataTypes.STRING(32),
            allowNull: false,
            defaultValue: "0",
            comment: "硬币-积分"
        },
        score: {
            type: DataTypes.FLOAT(2, 1),
            allowNull: false,
            defaultValue: 0.0,
            comment: "评分"
        },
        scorer: {
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 0,
            comment: "评分人数"
        },
        status: {
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 1,
            comment: "状态1 正常 2失效"
        },
        create_time: {
            type: DataTypes.INTEGER,
            allowNull: false,
            comment: "创建时间"
        },
        update_time: {
            type: DataTypes.INTEGER,
            allowNull: true,
            comment: "更新时间"
        }
    }, {
        tableName: 'booklist',
        timestamps: false,
        indexes: [
            {
                name: "PRIMARY",
                unique: true,
                using: "BTREE",
                fields: [
                    { name: "id" },
                ]
            },
        ]
    });

    // 多表查询
    Booklist.associate = function () {
        // Booklist与User是一对一关系，外键在源模型中定义 所以这里使用belongsTo()
        app.model.Booklist.belongsTo(app.model.User, {
            as: 'userInfo',
            foreignKey: 'user_id'
        });

        app.model.Booklist.hasMany(app.model.BooklistCategory, {
            as: 'categorys',
            sourceKey: 'id',
            foreignKey: 'booklist_id'
        });
    }

    return Booklist;
};