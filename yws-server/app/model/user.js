module.exports = app => {
    // 获取数据类型
    const DataTypes = app.Sequelize;

    // 定义模型
    const User = app.model.define('user', {
        id: {
            autoIncrement: true,
            type: DataTypes.INTEGER,
            allowNull: false,
            primaryKey: true,
            comment: "自增id"
        },
        name: {
            type: DataTypes.STRING(64),
            allowNull: false,
            comment: "会员昵称"
        },
        head_img_url: {
            type: DataTypes.STRING(128),
            allowNull: true,
            comment: "头像"
        },
        sex: {
            type: DataTypes.CHAR(1),
            allowNull: false,
            defaultValue: "0",
            comment: "性别：0未设置 1男 2女"
        },
        type: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: 1,
            comment: "注册方式：1邮箱(默认) 2手机号 3第三方唯一标识"
        },
        email: {
            type: DataTypes.STRING(32),
            allowNull: true,
            comment: "邮箱-可用于登录",
            unique: "INDEX_E"
        },
        mobile: {
            type: DataTypes.CHAR(11),
            allowNull: true,
            comment: "手机号-可用于登录",
            unique: "INDEX_M"
        },
        third_id: {
            type: DataTypes.CHAR(11),
            allowNull: true,
            comment: "第三方唯一标识-可用于登录"
        },
        password: {
            type: DataTypes.STRING(32),
            allowNull: false,
            comment: "登录密码加密串"
        },
        score_level: {
            type: DataTypes.INTEGER,
            allowNull: true,
            comment: "评分级别 1 2 3"
        },
        point: {
            type: DataTypes.STRING(32),
            allowNull: true,
            defaultValue: "0",
            comment: "积分"
        },
        status: {
            type: DataTypes.TINYINT.UNSIGNED,
            allowNull: false,
            defaultValue: 1,
            comment: "状态：0冻结 1正常 2禁言"
        },
        create_time: {
            type: DataTypes.INTEGER,
            allowNull: false,
            comment: "创建时间"
        },
        update_time: {
            type: DataTypes.INTEGER,
            allowNull: true,
            comment: "登录时间"
        }
    }, {
        tableName: 'user',
        timestamps: false,
        indexes: [
            {
                name: "PRIMARY",
                unique: true,
                using: "BTREE",
                fields: [
                    { name: "id" },
                ]
            },
            {
                name: "INDEX_M",
                unique: true,
                using: "BTREE",
                fields: [
                    { name: "mobile" },
                ]
            },
            {
                name: "INDEX_E",
                unique: true,
                using: "BTREE",
                fields: [
                    { name: "email" },
                ]
            },
        ]
    });

    // User.associate = function () {
    //     app.model.User.hasMany(app.model.NovelDiscuss, {
    //         foreignKey: 'user_id',
    //         otherKey: 'res_id'
    //     });
    // }

    return User;
};