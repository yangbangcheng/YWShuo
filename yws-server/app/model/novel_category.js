module.exports = app => {
    // 获取数据类型
    const DataTypes = app.Sequelize;

    // 定义模型
    const NovelCategory = app.model.define('novel_category', {
        id: {
            autoIncrement: true,
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: false,
            primaryKey: true
        },
        type: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: false,
            defaultValue: 1,
            comment: "1男频 2女频"
        },
        cate_name: {
            type: DataTypes.STRING(100),
            allowNull: false,
            comment: "分类名称"
        },
        thumb: {
            type: DataTypes.STRING(255),
            allowNull: true,
            comment: "缩略图"
        },
        sorts: {
            type: DataTypes.INTEGER.UNSIGNED,
            allowNull: false,
            defaultValue: 0,
            comment: "排序值"
        },
        status: {
            type: DataTypes.TINYINT.UNSIGNED,
            allowNull: false,
            defaultValue: 1,
            comment: "状态 0禁用  1启用"
        },
        create_time: {
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 1609825010,
            comment: "添加时间"
        },
        update_time: {
            type: DataTypes.INTEGER,
            allowNull: true,
            defaultValue: 1609825010,
            comment: "更新时间"
        }
    }, {
        tableName: 'novel_category',
        timestamps: false,
        indexes: [
            {
                name: "PRIMARY",
                unique: true,
                using: "BTREE",
                fields: [
                    { name: "id" },
                ]
            },
            {
                name: "index_p",
                using: "BTREE",
                fields: [
                    { name: "type" },
                ]
            },
        ]
    });

    return NovelCategory;
};