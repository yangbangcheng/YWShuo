'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
    const { router, controller } = app;
    const { email, novel, discuss, booklist, user, praise, bookshelf, userPointRecord, task } = controller

    const jwt = app.middleware.jwt();
    const decodeToken = app.middleware.decodeToken()
    const error = app.middleware.error()

    // 邮箱发送邮件
    router.post('/api/email/sendMail', error, email.sendMail);

    // api
    // 小说搜索
    router.get('/api/novel/search', error, novel.search);
    // 获取小说列表
    router.get('/api/novel/getList', error, novel.getList);
    // 获取小说分类
    router.get('/api/novel/getCategory', error, novel.getCategory);
    // 获取小说详情
    router.get('/api/novel/getInfo', error, novel.getInfo);

    // 获取小说评论列表
    router.get('/api/novel/getDiscussList', error, novel.getDiscussList);
    // 随机获取小说评论
    router.get('/api/novel/getRandomDiscuss', error, novel.getRandomDiscuss);
    // 获取用户小说评论详情
    router.get('/api/novel/getDiscussInfo', jwt, error, novel.getDiscussInfo);
    // 发布小说评论
    router.post('/api/novel/postDiscuss', jwt, error, novel.postDiscuss);
    // 修改书籍标签
    router.post('/api/novel/editTag', jwt, error, novel.editTag);
    // 获取用户小说标签
    router.get('/api/novel/getUserTagList', jwt, error, novel.getUserTagList);
    // 书籍打赏
    router.post('/api/novel/addPoint', jwt, error, novel.addPoint);

    // 获取书籍评论回复列表
    router.get('/api/discuss/getReply', error, discuss.getReply);
    // 回复评论
    router.post('/api/discuss/postReply', jwt, error, discuss.postReply);

    // 获取书单列表
    router.get('/api/booklist/getList', error, booklist.getList);
    // 获取书单信息
    router.get('/api/booklist/getInfo', error, booklist.getInfo);
    // 创建书单
    router.post('/api/booklist/create', jwt, error, booklist.create);
    // 添加书籍到书单
    router.post('/api/booklist/addNovel', jwt, error, booklist.addNovel);
    // 获取书单书籍列表
    router.get('/api/booklist/getNovelList', decodeToken, error, booklist.getNovelList);
    // 获取书单评论
    router.get('/api/booklist/getDiscussList', error, booklist.getDiscussList);
    // 发布书单评论
    router.post('/api/booklist/createDiscuss', jwt, error, booklist.createDiscuss);

    // 用户注册
    router.post('/api/user/register', error, user.register);
    // 用户登录
    router.post('/api/user/signIn', error, user.signIn);
    // 获取用户信息
    router.get('/api/user/getInfo', jwt, error, user.getInfo);

    // 点赞或点赞
    router.post('/api/praise/setStatus', jwt, error, praise.setStatus);

    // 书架
    router.post('/api/bookshelf/setStatus', jwt, error, bookshelf.setStatus);
    router.get('/api/bookshelf/getNoverList', jwt, error, bookshelf.getNoverList);
    router.get('/api/bookshelf/getNoverStatus', jwt, error, bookshelf.getNoverStatus);

    // 硬币
    // 获取流水
    router.get('/api/userPointRecord/getList', jwt, error, userPointRecord.getList);

    // 任务
    router.get('/api/task/getList', jwt, error, task.getList);
    // 签到
    router.post('/api/task/signIn', jwt, error, task.signIn);
};
