const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('user_point_record', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      comment: "自增id"
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "会员编号"
    },
    target: {
      type: DataTypes.INTEGER,
      allowNull: true,
      comment: "目标 1书籍 暂时只有1"
    },
    target_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      comment: "目标id"
    },
    type: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      comment: "变更类型：1增加 2减少"
    },
    before_point: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "变更前积分"
    },
    change_num: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "变更值"
    },
    after_point: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "变更后积分"
    },
    desc: {
      type: DataTypes.STRING(64),
      allowNull: true,
      comment: "变更说明"
    },
    status: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: 1,
      comment: "状态：1正常(默认) 0作废"
    },
    operate_admin_idx: {
      type: DataTypes.INTEGER,
      allowNull: true,
      comment: "操作人管理员编号"
    },
    operate_desc: {
      type: DataTypes.STRING(64),
      allowNull: true,
      comment: "操作说明"
    },
    update_time: {
      type: DataTypes.INTEGER,
      allowNull: true,
      comment: "操作时间"
    },
    create_time: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "创建时间"
    }
  }, {
    sequelize,
    tableName: 'user_point_record',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "INDEX_M",
        using: "BTREE",
        fields: [
          { name: "user_id" },
        ]
      },
    ]
  });
};
