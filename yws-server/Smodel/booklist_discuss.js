const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('booklist_discuss', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      comment: "自增id"
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "用户id"
    },
    parent_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      comment: "父(主评论)编号"
    },
    res_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      comment: "被回复人会员编号（有二级以上评论时使用）"
    },
    booklist_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "书单id"
    },
    content: {
      type: DataTypes.TEXT,
      allowNull: false,
      comment: "评论内容"
    },
    dz_num: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0,
      comment: "点赞数量"
    },
    c_num: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
      comment: "踩数量"
    },
    reply_num: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
      comment: "下级评论数"
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1,
      comment: "状态：1显示 2不显示 0无效"
    },
    create_time: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "创建时间"
    },
    update_time: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "更新时间"
    }
  }, {
    sequelize,
    tableName: 'booklist_discuss',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
