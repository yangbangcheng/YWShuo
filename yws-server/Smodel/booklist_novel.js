const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('booklist_novel', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      comment: "自增id"
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "用户编号"
    },
    booklist_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "书单id"
    },
    discuss_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "评论id"
    },
    category_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "分类id"
    },
    novel_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "书籍id"
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1,
      comment: "状态1 正常 2失效"
    },
    create_time: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "创建时间"
    },
    update_time: {
      type: DataTypes.INTEGER,
      allowNull: true,
      comment: "更新时间"
    }
  }, {
    sequelize,
    tableName: 'booklist_novel',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
