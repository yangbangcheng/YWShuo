const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('task_record', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      comment: "自增id"
    },
    task_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "任务id"
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "用户id"
    },
    count: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "总完成次数"
    },
    continue_count: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "连续完成次数"
    },
    point: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
      comment: "奖励的积分"
    },
    desc: {
      type: DataTypes.STRING(255),
      allowNull: true,
      comment: "备注"
    },
    create_time: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "创建时间"
    },
    update_time: {
      type: DataTypes.INTEGER,
      allowNull: true,
      comment: "更新时间"
    }
  }, {
    sequelize,
    tableName: 'task_record',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
