var DataTypes = require("sequelize").DataTypes;
var _admin = require("./admin");
var _banner = require("./banner");
var _booklist = require("./booklist");
var _booklist_category = require("./booklist_category");
var _booklist_discuss = require("./booklist_discuss");
var _booklist_novel = require("./booklist_novel");
var _bookshelf = require("./bookshelf");
var _novel = require("./novel");
var _novel_category = require("./novel_category");
var _novel_discuss = require("./novel_discuss");
var _novel_tag = require("./novel_tag");
var _praise = require("./praise");
var _task = require("./task");
var _task_record = require("./task_record");
var _user = require("./user");
var _user_novel_tag = require("./user_novel_tag");
var _user_point_record = require("./user_point_record");

function initModels(sequelize) {
  var admin = _admin(sequelize, DataTypes);
  var banner = _banner(sequelize, DataTypes);
  var booklist = _booklist(sequelize, DataTypes);
  var booklist_category = _booklist_category(sequelize, DataTypes);
  var booklist_discuss = _booklist_discuss(sequelize, DataTypes);
  var booklist_novel = _booklist_novel(sequelize, DataTypes);
  var bookshelf = _bookshelf(sequelize, DataTypes);
  var novel = _novel(sequelize, DataTypes);
  var novel_category = _novel_category(sequelize, DataTypes);
  var novel_discuss = _novel_discuss(sequelize, DataTypes);
  var novel_tag = _novel_tag(sequelize, DataTypes);
  var praise = _praise(sequelize, DataTypes);
  var task = _task(sequelize, DataTypes);
  var task_record = _task_record(sequelize, DataTypes);
  var user = _user(sequelize, DataTypes);
  var user_novel_tag = _user_novel_tag(sequelize, DataTypes);
  var user_point_record = _user_point_record(sequelize, DataTypes);


  return {
    admin,
    banner,
    booklist,
    booklist_category,
    booklist_discuss,
    booklist_novel,
    bookshelf,
    novel,
    novel_category,
    novel_discuss,
    novel_tag,
    praise,
    task,
    task_record,
    user,
    user_novel_tag,
    user_point_record,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
