const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('novel_discuss', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      comment: "自增id"
    },
    parent_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      comment: "父(主评论)编号"
    },
    novel_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "书编号 novel.id"
    },
    score: {
      type: DataTypes.CHAR(2),
      allowNull: true,
      comment: "书籍评分"
    },
    score_level: {
      type: DataTypes.INTEGER,
      allowNull: true,
      comment: "评分级别 1 2 3"
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "会员编号 user.id"
    },
    res_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      comment: "被回复人会员编号（有二级以上评论时使用）"
    },
    content: {
      type: DataTypes.TEXT,
      allowNull: false,
      comment: "评论内容"
    },
    power: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: "0",
      comment: "权重"
    },
    point: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: "0",
      comment: "硬币-积分"
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1,
      comment: "状态：1显示 2不显示 0无效"
    },
    create_time: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "创建时间"
    },
    update_time: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "更新时间"
    },
    is_ys: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
      comment: "是不是优书网评论 0不是 1是"
    }
  }, {
    sequelize,
    tableName: 'novel_discuss',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
