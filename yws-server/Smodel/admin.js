const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('admin', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      comment: "自增idx"
    },
    admin_name: {
      type: DataTypes.STRING(32),
      allowNull: true,
      comment: "管理员名称"
    },
    admin_phone: {
      type: DataTypes.CHAR(11),
      allowNull: true,
      comment: "管理员手机号"
    },
    login_account: {
      type: DataTypes.STRING(64),
      allowNull: false,
      comment: "登录账号",
      unique: "INDEX_L"
    },
    login_password: {
      type: DataTypes.STRING(64),
      allowNull: false,
      comment: "登陆密码"
    },
    is_admin: {
      type: DataTypes.CHAR(1),
      allowNull: false,
      defaultValue: "0",
      comment: "是否是超级管理员 1是"
    },
    create_time: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "添加时间"
    },
    update_time: {
      type: DataTypes.INTEGER,
      allowNull: true,
      comment: "登录时间"
    }
  }, {
    sequelize,
    tableName: 'admin',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "INDEX_L",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "login_account" },
        ]
      },
    ]
  });
};
