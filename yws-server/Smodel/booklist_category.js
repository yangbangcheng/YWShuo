const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('booklist_category', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      comment: "自增id"
    },
    booklist_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "书单id"
    },
    category_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "分类id"
    },
    category_name: {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: "分类名称"
    },
    count: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1,
      comment: "计数"
    },
    create_time: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "创建时间"
    },
    update_time: {
      type: DataTypes.INTEGER,
      allowNull: true,
      comment: "更新时间"
    }
  }, {
    sequelize,
    tableName: 'booklist_category',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
