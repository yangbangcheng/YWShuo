const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('bookshelf', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      comment: "自增id"
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "用户id"
    },
    type: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1,
      comment: "类型 1 正在追读 2 养肥待看 3 已经看过"
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1,
      comment: "状态  1有效 2移除"
    },
    novel_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "书编号 novel.id"
    },
    create_time: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "创建时间"
    },
    update_time: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "更新时间"
    }
  }, {
    sequelize,
    tableName: 'bookshelf',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
