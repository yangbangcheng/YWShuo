const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('praise', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      comment: "自增id"
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "用户id"
    },
    type: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1,
      comment: "类型 1 赞 2 踩"
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1,
      comment: "状态  1有效 2取消"
    },
    target: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1,
      comment: "目标 1评论 暂时只有1"
    },
    target_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "目标的id"
    },
    create_time: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "创建时间"
    },
    update_time: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "更新时间"
    }
  }, {
    sequelize,
    tableName: 'praise',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
