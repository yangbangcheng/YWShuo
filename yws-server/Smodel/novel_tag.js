const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('novel_tag', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      comment: "自增id"
    },
    tag_name: {
      type: DataTypes.STRING(64),
      allowNull: false,
      comment: "标签名"
    },
    novel_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "书籍id"
    },
    add_count: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1,
      comment: "添加人数"
    },
    status: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: 1,
      comment: "状态：1正常(默认) 0作废"
    },
    update_time: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 1610682634,
      comment: "更新时间"
    }
  }, {
    sequelize,
    tableName: 'novel_tag',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
