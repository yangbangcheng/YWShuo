const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('novel', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      comment: "自增id"
    },
    novel_name: {
      type: DataTypes.STRING(64),
      allowNull: false,
      comment: "书名"
    },
    category_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "分类id"
    },
    novel_img: {
      type: DataTypes.STRING(124),
      allowNull: true,
      comment: "书封面图"
    },
    author_name: {
      type: DataTypes.STRING(36),
      allowNull: false,
      comment: "作者"
    },
    synopsis: {
      type: DataTypes.TEXT,
      allowNull: true,
      comment: "简介"
    },
    word_number: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "字数"
    },
    update_status: {
      type: DataTypes.TINYINT.UNSIGNED,
      allowNull: false,
      defaultValue: 0,
      comment: "书当前更新状态：0连载中 1已完结 2歇菜"
    },
    update_explain: {
      type: DataTypes.STRING(64),
      allowNull: true,
      comment: "更新说明，例：每周二21:00更新"
    },
    status: {
      type: DataTypes.TINYINT.UNSIGNED,
      allowNull: false,
      defaultValue: 2,
      comment: "状态：0下架 1审核中 2上架"
    },
    source: {
      type: DataTypes.TEXT,
      allowNull: false,
      comment: "源站地址和id"
    },
    power: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: "0",
      comment: "权重"
    },
    point: {
      type: DataTypes.STRING(32),
      allowNull: false,
      defaultValue: "0",
      comment: "硬币-积分"
    },
    score: {
      type: DataTypes.FLOAT(2,1),
      allowNull: false,
      defaultValue: 0.0,
      comment: "综合评分"
    },
    scorer: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
      comment: "综合评分人数"
    },
    score_1: {
      type: DataTypes.FLOAT(2,1),
      allowNull: false,
      defaultValue: 0.0,
      comment: "一级评分"
    },
    scorer_1: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
      comment: "一级评分人数"
    },
    score_2: {
      type: DataTypes.FLOAT(2,1),
      allowNull: false,
      defaultValue: 0.0,
      comment: "二级评分"
    },
    scorer_2: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
      comment: "二级评分人数"
    },
    score_3: {
      type: DataTypes.FLOAT(2,1),
      allowNull: false,
      defaultValue: 0.0,
      comment: "三级评分"
    },
    scorer_3: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
      comment: "三级评分人数"
    },
    create_time: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: "记录创建时间"
    },
    update_time: {
      type: DataTypes.INTEGER,
      allowNull: true,
      comment: "小说更新时间"
    }
  }, {
    sequelize,
    tableName: 'novel',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
